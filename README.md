# README #

##Mobile application##

Mobile application is stored in the "mobile app" folder. To run the app you must install node.js and npm on your computer and follow the next guide:https://facebook.github.io/react-native/docs/getting-started.html

##Server app##

Server application in stored int the "server" folder. You need npm and node.js installed on your computer to run the app.
To run server, please follow the following steps:

1. cd your_repository_path/server
2. npm install
3. node server.js

##Homework##

All homeworks written in Python are stored in the "homework" folder. You should have Python installed on your computer.