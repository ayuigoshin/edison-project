import time
import pyupm_i2clcd
import pyupm_grove as grove
import pyupm_buzzer as upmBuzzer


# temp = grove.GroveTemp(0)
rotary = grove.GroveRotary(0)
lcd = pyupm_i2clcd.Jhd1313m1(6, 0x3E, 0x62)
buzzer = upmBuzzer.Buzzer(5)

delay = 1

alerted = False
alertDuration = 1 * 1000 * 1000

MAX_ANGLE = 30
MIN_ANGLE = 0
buzzer.stopSound()
while True:
    lcd.clear()
    # celsius = temp.value()
    c = int(rotary.abs_deg() / 10)
    color = int(255 * (c - MIN_ANGLE) / (MAX_ANGLE - MIN_ANGLE))
    if MIN_ANGLE < c < MAX_ANGLE-1:
        alerted = False
    elif not alerted:
        buzzer.playSound(upmBuzzer.DO, alertDuration)
        alerted = True
        buzzer.stopSound()
    # if c < 0:
    #     c = 0
    # elif c > 255:
    #     c = 255
    print(color)
    lcd.setColor(color, 0, 255 - color)
    lcd.clear()
    lcd.setCursor(0, 0)
    # lcd.write('%d%cC' % (celsius, chr(223)))
    lcd.write(str(c))

    time.sleep(delay)

