import mraa
import time

state = {
    "stop": False,
    "position": 0,
    "pause": False
}

OCTAVE4_FREQUENCIES = (261.626, 277.183, 293.665, 311.127, 329.628, 349.228, 369.994, 391.995,
                       415.305, 440.0, 466.164, 493.883, 0)
NOTE_NAMES = {'c': 0, 'c#': 1, 'd': 2, 'd#': 3, 'e': 4, 'f': 5, 'f#': 6, 'g': 7, 'g#': 8, 'a': 9,
              'a#': 10, 'b': 11, 'h': 11, 'p': 12}
ALLOWED_NOTES = ('c#', 'c', 'd#', 'd', 'e', 'f#', 'f', 'g#', 'g', 'a#', 'a', 'b', 'h', 'p')


def remove_extra(string):
    tmp = string.replace(" ", "")
    return tmp.replace("\n", "")


def get_defaults(defaults_string):
    defaults_list = defaults_string.split(',')
    defaults_dict = dict([default.split('=') for default in defaults_list])
    defaults = {'duration': int(defaults_dict['d']), 'octave': int(defaults_dict['o']), 'bpm':
        int(defaults_dict['b'])}
    return defaults


buzzer = mraa.Pwm(5)


def play(on_stop):
    state["pause"] = False
    buzzer.write(0)
    buzzer.enable(True)
    volume_level = 0.2
    source = open("/home/root/bumer.rtttl", "rt")
    track = remove_extra(source.readline())
    source.close()
    parts = track.split(':')
    print(parts)
    track_name = parts[0]
    defaults = get_defaults(parts[1])
    data_list = parts[2].split(',')
    beat = 60.0 / float(defaults['bpm'])
    print("'%s' is playing..." % track_name)
    
    for pos in range(state["position"], len(data_list)):
        data = data_list[pos]
        for allowed_note in ALLOWED_NOTES:
            note_position = data.find(allowed_note)
            print(data)
            if note_position != -1:
                note = allowed_note
                break
        if state["stop"]:
            buzzer.enable(False)
            state["position"] = 0
            state["stop"] = False
            return
        
        if state["pause"]:
            state["position"] = pos
            buzzer.enable(False)
            return

        if len(data[:note_position]) > 0:
            duration = int(data[:note_position])
        else:
            duration = defaults['duration']
        
        real_duration = 60.0 / defaults['bpm'] / duration  # in seconds
        if data.find('.') != -1:
            real_duration += real_duration / 2.0
            data = data.replace(".", "")
        if len(data[note_position + len(note):]) > 0:
            octave = int(data[note_position + len(note):])
        else:
            octave = defaults['octave']
        
        frequency = OCTAVE4_FREQUENCIES[NOTE_NAMES[note]] * (2 ** (octave - 4))
        
        if frequency != 0:
            buzzer.period_us(int((10 ** 6) / frequency))
            buzzer.write(volume_level)
        else:
            buzzer.write(0)
        
        time.sleep(real_duration)
        buzzer.write(0)
        time.sleep(beat - real_duration)
    buzzer.enable(False)
    on_stop()


def stop():
    state["stop"] = True
    
    
def pause():
    state["pause"] = True
