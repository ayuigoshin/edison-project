import paho.mqtt.client as mqtt
import json
import music
import pyupm_i2clcd
import pyupm_grove as grove
import mraa
import threading

lcd = pyupm_i2clcd.Jhd1313m1(6, 0x3E, 0x62)
lcd.setColor(0, 0, 255)
lcd.clear()

temp = grove.GroveTemp(0)
celsius = temp.value()


redLed = mraa.Gpio(2)
greenLed = mraa.Gpio(3)
blueLed = mraa.Gpio(4)

redLed.dir(mraa.DIR_OUT)
greenLed.dir(mraa.DIR_OUT)
blueLed.dir(mraa.DIR_OUT)

redLed.write(0)
greenLed.write(0)
blueLed.write(0)

config = {
    "temperature": 0,
    "led": {
        "red": 0,
        "green": 0,
        "blue": 0
    },
    "music": "stop"
}

config["temperature"] = temp.value()


def writeRedButton(state=1):
    config["led"]["red"] = state
    redLed.write(state)
    print(state)


def writeGreenButton(state=1):
    config["led"]["green"] = state
    greenLed.write(state)
    print(state)


def writeBlueButton(state=1):
    config["led"]["blue"] = state
    blueLed.write(state)
    print(state)
    
    
def sendConfig(client):
    val = temp.value()
    config["temperature"] = val
    client.publish("/data/0/cfg/fb", payload=json.dumps(config), qos=0, retain=False)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/data/0/temp/rq")
    client.subscribe("/data/0/led/red/rq")
    client.subscribe("/data/0/led/blue/rq")
    client.subscribe("/data/0/led/green/rq")
    client.subscribe("/data/0/music/rq")
    client.subscribe("/data/0/msg/rq")
    sendConfig(client=client)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    topic = str(msg.topic)
    data = str(msg.payload)
    if topic == "/data/0/cfg/rq":
        sendConfig(client=client)
    elif topic == "/data/0/temp/rq":
        val = temp.value()
        config["temperature"] = val
        client.publish("/data/0/temp/fb", payload=json.dumps({'value': val}), qos=0, retain=False)
    
    elif topic == "/data/0/led/red/rq":
        data = json.loads(msg.payload)
        writeRedButton(data["value"])
        client.publish("/data/0/led/red/fb", payload=json.dumps({'value': data["value"]}), qos=0, retain=False)
    
    elif topic == "/data/0/led/green/rq":
        data = json.loads(msg.payload)
        writeGreenButton(data["value"])
        client.publish("/data/0/led/green/fb", payload=json.dumps({'value': data["value"]}), qos=0, retain=False)
    
    elif topic == "/data/0/led/blue/rq":
        data = json.loads(msg.payload)
        writeBlueButton(data["value"])
        client.publish("/data/0/led/blue/fb", payload=json.dumps({'value': data["value"]}), qos=0, retain=False)
    
    elif topic == "/data/0/music/rq":
        data = json.loads(msg.payload)
        if data["state"] == "play":
            thread = threading.Thread(target=music.play, args=(lambda : client.publish("/data/0/music/fb", payload=json.dumps({'state': "stop"}), qos=0, retain=False),))
            thread.start()
            # music.play()
            
            print("music.play")
        elif data["state"] == "stop":
            music.stop()
        elif data["state"] == "pause":
            music.pause()
        else:
            return
        config["music"] = data["state"]
        client.publish("/data/0/music/fb", payload=json.dumps({'state': data["state"]}), qos=0, retain=False)
    
    elif topic == "/data/0/msg/rq":
        data = json.loads(msg.payload)
        print(data)
        lcd.clear()
        lcd.setCursor(0, 0)
        lcd.write(str(data["msg"]))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("10.42.0.1", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
