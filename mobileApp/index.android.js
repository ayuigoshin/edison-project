/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    Alert,
    TextInput
} from 'react-native';
var SocketIOClient = require('socket.io-client/dist/socket.io');
import axios from 'axios';

export default class mobileApp extends Component {

    constructor() {
        super();
        this.state = {
            url: '10.0.2.2:3000',
            connected: false,
            cache: {
                temperature: null,
                led: {
                    red: null,
                    green: null,
                    blue: null
                },
                music: null
            },
            msg: ''
        };
        this.color = "#841584";

    }

    toggleGreen() {
        axios.post('http://' + this.state.url + '/api/led/green', {value: this.state.cache.led.green ? 0 : 1})
    }
    toggleRed() {
        axios.post('http://' + this.state.url + '/api/led/red', {value: this.state.cache.led.red ? 0 : 1})
    }
    toggleBlue() {
        axios.post('http://' + this.state.url + '/api/led/blue', {value: this.state.cache.led.blue ? 0 : 1})
    }

    toggleMusic() {
        axios.post('http://' + this.state.url + '/api/music', {state: this.state.cache.music === 'play' ? 'pause' : 'play'});
    }

    stopMusic() {
        axios.post('http://' + this.state.url + '/api/music', {state: "stop"});
    }

    sendMsg() {
        axios.post('http://' + this.state.url + '/api/msg', {msg: this.state.msg})
    }


    connectSocket() {
        this.socket = SocketIOClient('http://' + this.state.url);
        this.socket.on('cache::update', (msg) => {
            const message = JSON.parse(msg);
            this.setState({cache: message});
        });
        this.socket.on('temperature::update', (msg) => {
            const message = JSON.parse(msg);
            this.setState({cache: {...this.state.cache, temperature: message.value}});
        });
        this.socket.on('led:red::update', (msg) => {
            const message = JSON.parse(msg);
            this.setState({cache: {...this.state.cache,
                led: {...this.state.cache.led, red: message.value}
            }});
        });
        this.socket.on('led:blue::update', (msg) => {
            const message = JSON.parse(msg);
            this.setState({cache: {...this.state.cache,
                led: {...this.state.cache.led, blue: message.value}
            }});
        });
        this.socket.on('led:green::update', (msg) => {
            const message = JSON.parse(msg);
            this.setState({cache: {...this.state.cache,
                led: {...this.state.cache.led, green: message.value}
            }});
        });
        this.socket.on('music:state::update', (msg) => {
            const message = JSON.parse(msg);
            this.setState({cache: {...this.state.cache,
                music:  message.state
            }});
        });
        this.setState({connected: true});
    }

    render() {

        var controls = <View>
                <View style={styles.row}>
                    <Button
                        onPress={this.toggleRed.bind(this)}
                        disabled={this.state.cache.led.red === null}
                        title={this.state.cache.led.red ? "Turn off Red" : "Turn on Red"}
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                    <Button
                        onPress={this.toggleBlue.bind(this)}
                        disabled={this.state.cache.led.blue === null}
                        title={this.state.cache.led.blue ? "Turn off Blue" : "Turn on Blue"}
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                    <Button
                        onPress={this.toggleGreen.bind(this)}
                        disabled={this.state.cache.led.green === null}
                        title={this.state.cache.led.green ? "Turn off Green" : "Turn on Green"}
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
                <View style={styles.row}>
                    <Text>Temperature: {this.state.cache.temperature}°C</Text>
                    <Button
                        onPress={this.stopMusic.bind(this)}
                        title="Stop music"
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                    <Button
                        onPress={this.toggleMusic.bind(this)}
                        title={this.state.cache.music === 'play' ? "Pause music" : "Play music"}
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
                <View style={styles.row}>
                    <TextInput
                        editable={this.state.connected}
                        style={styles.input}
                        onChangeText={(text) => this.setState({msg: text})}
                        value={this.state.msg}
                    />
                    <Button
                        onPress={this.sendMsg.bind(this)}
                        title="Send"
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
                <View style={styles.row}>
                    <Button
                        onPress={() => {axios.get('http://' + this.state.url + '/api/temp')}}
                        title="Update Temperature"
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
            </View>;


        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Text>Url:</Text>
                    <TextInput
                        editable={!this.state.connected}
                        style={styles.input}
                        onChangeText={(text) => this.setState({url: text})}
                        value={this.state.url}
                    />
                    <Button
                        disabled={this.state.connected}
                        onPress={this.connectSocket.bind(this)}
                        title="Connect"
                        color={this.color}
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
                { (() => this.state.connected ? controls : null)()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    column: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 2
    },
    row: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 2,
        alignSelf: 'stretch'
    },
    input: {
        flex: 1,
        marginRight: 5,
        marginLeft: 5,
        borderColor: this.color
    }
});

AppRegistry.registerComponent('mobileApp', () => mobileApp);
