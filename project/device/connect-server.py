import paho.mqtt.client as mqtt
import json
import threading
import time
import accelerometer_util
import vibration_sensor_util
import gps_util
import lcd_util


def get_device_info(gps_sensor, accelerometer, vibration_sensor):
    gprmc, gpgga = gps_util.get_gps_info(gps_sensor)

    accelerometer_raw_data = accelerometer_util.get_accelerometer_info(accelerometer)
    accelerometer_info = {"x": accelerometer_raw_data[0],
                          "y": accelerometer_raw_data[1],
                          "z": accelerometer_raw_data[2]}

    vibration_info = vibration_sensor_util.get_vibration_sensor_info(vibration_sensor)

    lcd_util.write_data(int(gpgga.split(',')[7]), vibration_info)
    print accelerometer_info

    return {
        "gps": gprmc,
        "accelerometer": accelerometer_info,
        "vibration": vibration_info
    }


def collect_and_send_data(gps_sensor, accelerometer, vibration_sensor):
    while True:
        data = get_device_info(gps_sensor, accelerometer, vibration_sensor)
        client.publish("/data/info/fb", payload=json.dumps(data), qos=0, retain=False)
        time.sleep(2)


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/data/info/rq")
    client.publish("/data", payload=json.dumps(config), qos=0, retain=False)

    thread = threading.Thread(target=collect_and_send_data, args=(gps_sensor, accelerometer, vibration_sensor,))
    thread.start()


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    if msg.topic == "/data/info/rq":
        data = get_device_info(gps_sensor, accelerometer, vibration_sensor)
        client.publish("/data/info/fb", payload=json.dumps(data), qos=0, retain=False)


# host = "10.42.0.1"
host = "192.168.43.29"

config = {
    "id": 0
}

gps_sensor = gps_util.init()
accelerometer = accelerometer_util.init()
vibration_sensor = vibration_sensor_util.init()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(host, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
