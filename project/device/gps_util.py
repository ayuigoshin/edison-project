def init():
    return open("/dev/ttyMFD1", "rt")


def get_gps_info(gps_sensor):
    gprmc = ""
    gpgga = ""
    while True:
        if gprmc == "" or gpgga == "":
            str = gps_sensor.readline()
            if str.startswith("$GPRMC"):
                gprmc = str
            if str.startswith("$GPGGA"):
                gpgga = str
        else:
            break
        # print str
        # print gpgga
        # print gprmc

    return gprmc, gpgga


def parse_GPGGA(gpgga_str):
    pass

if __name__ == "__main__":
    gps_sensor = init()
    while True:
        print get_gps_info(gps_sensor)
