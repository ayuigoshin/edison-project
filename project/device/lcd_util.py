import pyupm_i2clcd

lcd = pyupm_i2clcd.Jhd1313m1(6, 0x3E, 0x62)

lcd.clear()


def write_satelines(count=0):
    lcd.setCursor(1, 0)
    lcd.write('|' * count)


def write_vibration(value):
    lcd.setCursor(0, 0)
    lcd.write(str(value))


def write_data(satelines_count, vibration_value):
    lcd.clear()
    write_satelines(satelines_count)
    write_vibration(vibration_value)

if __name__ == '__main__':
    print '|' * 8
