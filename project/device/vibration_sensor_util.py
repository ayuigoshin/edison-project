from __future__ import print_function

import pyupm_ldt0028 as ldt0028


def init():
    return ldt0028.LDT0028(0)


def get_vibration_sensor_info(sensor):
    sum = 0
    for i in xrange(50):
        sum += sensor.getSample()
    average = sum / 50

    return average
