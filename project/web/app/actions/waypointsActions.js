import axios from 'axios'
import {REST_CONTEXT} from '../services/constants'


export const GET_WAYPOINTS  = 'GET_WAYPOINTS';
export const ADD_WAYPOINT  = 'ADD_WAYPOINT';


export function getWaypoints() {
    return {
        type: GET_WAYPOINTS,
        payload: axios.get(REST_CONTEXT + '/data')
    }
}

export function addWypoint(waypoint) {
    return {
        type: ADD_WAYPOINT,
        payload: waypoint
    }
}
