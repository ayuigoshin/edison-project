import React from 'react';
import ReactDom from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Layout from './layout';

injectTapEventPlugin();

export default class App extends React.Component {

    render() {
        return (
            <MuiThemeProvider>
                <Layout />
            </MuiThemeProvider>
        );
    }
}

ReactDom.render(<App/>, document.getElementById('app'));