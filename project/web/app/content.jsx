import React from 'react';
import {Map, TileLayer, LayersControl, Marker} from 'react-leaflet';
import {GoogleLayer} from 'react-leaflet-google/dist/react-leaflet-google'
const { BaseLayer} = LayersControl;
import Route from './leaflet-route'
const key = 'Your Key goes here';
const terrain = 'TERRAIN';
const road = 'ROADMAP';
const satellite = 'SATELLITE';
const hydrid = 'HYBRID';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
L.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/';
import devicesStore from 'stores/waypointsStore';

export default class Content extends React.Component {

    static propTypes = {
        style: React.PropTypes.object,
        data: React.PropTypes.array
    };
    constructor(props) {
        super(props);
        this.getRoutes = this.getRoutes.bind(this);
    }

    getWaypoints() {
        return [
            {
                latitude: 57.74,
                longitude: 11.94,
                status:4
            },
            {
                latitude: 57.6792,
                longitude: 11.949,
                status: 5
            },
            {
                latitude: 57.68,
                longitude: 11.948,
                status: 3

            },
            {
                latitude: 57.68,
                longitude: 11.945,
                status: 2
            }
        ];
    }
    getRoutes() {

        return this.props.data.reduce((prev, next, array) => {
            if (!prev.length) {
                if (next.status > 5) {
                    return [...prev, {
                        color: 'red',
                        waypoints: [L.latLng([next.latitude, next.longitude])],
                        criteria: (item) => (item > 5)
                    }];
                } else if (next.status > 2) {
                    return [...prev, {
                        color: 'yellow',
                        waypoints: [L.latLng([next.latitude, next.longitude])],
                        criteria: (item) => (item > 2 && item <= 5)
                    }];
                } else {
                    return [...prev, {
                        color: 'green',
                        waypoints: [L.latLng([next.latitude, next.longitude])],
                        criteria: (item) => (item < 3)
                    }];
                }
            }
            let lastRoute = prev[prev.length  - 1];
            if (lastRoute.criteria(next.status)) {
                lastRoute.waypoints.push(L.latLng([next.latitude, next.longitude]));
                return prev;
            } else {
                if (next.status > 5) {
                    return [...prev, {
                        color: 'red',
                        waypoints: [lastRoute.waypoints[lastRoute.waypoints.length - 1], L.latLng([next.latitude, next.longitude])],
                        criteria: (item) => (item > 5)
                    }];
                } else if (next.status > 2) {
                    return [...prev, {
                        color: 'yellow',
                        waypoints: [lastRoute.waypoints[lastRoute.waypoints.length - 1], L.latLng([next.latitude, next.longitude])],
                        criteria: (item) => (item > 2 && item <= 5)
                    }];
                } else {
                    return [...prev, {
                        color: 'green',
                        waypoints: [lastRoute.waypoints[lastRoute.waypoints.length - 1], L.latLng([next.latitude, next.longitude])],
                        criteria: (item) => (item < 3)
                    }];
                }
            }

        }, [])
    }

    render() {
        const position = [0, 0];
        const googleKey = '';
        const routes = this.getRoutes();
        return (
            <div style={{...this.props.style, height: '100%'}}>
                <Map style={{height:' 100%'}}
                     center={position} zoom={2}
                     bounds={this.props.data.map(wp => L.latLng([wp.latitude, wp.longitude]))}
                >
                    <LayersControl position='topright'>
                        <BaseLayer  name='OpenStreetMap.Mapnik'>
                            <TileLayer  url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"/>
                        </BaseLayer>
                        <BaseLayer checked name='Google Maps Roads'>
                            <GoogleLayer googlekey={googleKey} maptype={road} />
                        </BaseLayer>
                    </LayersControl>
                    {routes.map((route, i) => <Route key={i} waypoints={route.waypoints} color={route.color}/>)}
                    {this.props.data.map((item, i) => item.vibration > 500 ? <Marker key={i} position={L.latLng([item.latitude, item.longitude])} /> : null)}
                </Map>
            </div>
        );
    }
}
