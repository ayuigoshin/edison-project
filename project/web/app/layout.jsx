import React from 'react'
import {AppBar, Drawer} from 'material-ui';
import classNames from 'classnames';
import Content from './content';
import SidebarContent from './sidebar-content';
import waypointsStore from 'stores/waypointsStore'
import {getWaypoints} from 'actions/waypointsActions'

export default class Layout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sidebarOpened: true,
            waypoints: []
        };
        this.toggleSidebar = this.toggleSidebar.bind(this);

    }

    toggleSidebar() {
        this.setState({...this.state, sidebarOpened: !this.state.sidebarOpened});
    }

    componentWillMount() {
        waypointsStore.subscribe(() => {
            this.setState({...this.state, waypoints: waypointsStore.getState().data});
        });
    }

    render() {
        const sidebarWidth = 250;

        return (
            <div
                style={{
                    height: 'calc(100% - 64px)'
                }}>
                <AppBar title={"Awesome Car Logger"} onLeftIconButtonTouchTap={this.toggleSidebar}/>
                <div
                    style={{
                        height: '100%',
                        position: 'relative'
                    }}>
                    <Drawer
                        open={this.state.sidebarOpened}
                        width={sidebarWidth}
                        containerStyle={{
                            position: 'absolute'
                        }}>
                        <SidebarContent
                            data={this.state.waypoints}/>
                    </Drawer>
                    <Content style={{
                        paddingLeft: this.state.sidebarOpened ? sidebarWidth : 0,
                        transition: 'padding 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
                    }} data={this.state.waypoints}/>
                </div>
            </div>
        );
    }
}