import React from 'react';
import 'leaflet-routing-machine'
const L = require('leaflet');
import Leaflet from 'leaflet';
import mapType from 'react-leaflet/src/types/map'
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css'

export default class LeafletRoute extends React.Component {
    static propTypes = {
        waypoints: React.PropTypes.array.isRequired,
        color: React.PropTypes.string
    };
    static defaultProps = {
        waypoints: [],
        color: 'black'
    };

    static contextTypes = {
        map: mapType,
    };


    constructor(props) {
        super(props);
        this.route = null;
        this.router = L.routing.osrmv1();
    }


    componentDidMount() {
        this.route = this.router.route(this.props.waypoints.map(latLng => L.routing.waypoint(latLng)), (err, routes) => {
            if (err) {
                return console.error(err);
            }
            this.route = L.routing.line(routes[0], {
                styles: [
                            {color: 'black', opacity: 0.15, weight: 9},
                            {color: 'white', opacity: 0.8, weight: 6},
                            {color: this.props.color, opacity: 1, weight: 2}
                        ]
            }).addTo(this.context.map)
        });

        // this.route = L.Routing.control({
        //     waypoints: this.props.waypoints,
        //     showAlternatives: false,
        //     draggableWaypoints: false,//to set draggable option to false
        //     addWaypoints: false,
        //     lineOptions: {
        //         styles: [
        //             {color: 'black', opacity: 0.15, weight: 9},
        //             {color: 'white', opacity: 0.8, weight: 6},
        //             {color: this.props.color, opacity: 1, weight: 2}
        //         ]
        //     },
        //     show: true,
        //     collapseBtn: null,
        //     collapsible: false,
        //     altLineOptions: {
        //         styles: [
        //             {color: 'black', opacity: 0, weight: 0},
        //             {color: 'black', opacity: 0, weight: 0},
        //             {color: 'black', opacity: 0, weight: 0}
        //         ]
        //     }
        // }).addTo(this.context.map);
    }

    render() {
        return this.props.children || null;
    }

    componentWillUnmount() {
        this.map.removeLayer(this.route);
    }
}