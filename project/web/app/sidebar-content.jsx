import React from 'react';
import {RaisedButton} from 'material-ui'
import axios from 'axios'
import {REST_CONTEXT} from 'services/constants'

export default class SidebarContent extends React.Component {

    static propTypes = {
        data: React.PropTypes.array
    };

    static defaultProps = {
        devices: [],
    };

    constructor(props) {
        super(props);
    }


    render() {

        const lastWp = this.props.data[this.props.data.length - 1];
        const data = lastWp ?
            (
                <div style={{
                    paddingLeft: 10
                }}>
                      <div style={{
                          padding: 3
                      }}>Lat: {lastWp.latitude}</div>
                    <div style={{
                        padding: 3
                    }}>Lng: {lastWp.longitude}</div>
                </div>

            ) : null;

        const averageSpeed = this.props.data.length ? this.props.data.map(item => item.speed).reduce((a, b) => a + b) / this.props.data.length : 0;
        const roadStatus = this.props.data.length ? this.props.data.map(item => item.status).reduce((a, b) => a + b) / this.props.data.length : 0;

        let roughness = 'Low';
        if (roadStatus > 5) {
            roughness = 'High';
        } else if (roadStatus > 2) {
            roughness = 'Middle';
        }

        return (
            <div>
                <div style={{
                    padding: 10,
                    color: '#aeaeae'
                }}>
                    <div>
                        Current position:
                    </div>
                    {data}
                    <div>Road roughness: {roughness}</div>
                    <div>Average speed: {Math.floor(averageSpeed * 1.8)} kmph </div>
                    <RaisedButton primary={true} label={"Get GPX"} href={REST_CONTEXT + '/data.gpx'}/>
                </div>
            </div>
        );
    }
}