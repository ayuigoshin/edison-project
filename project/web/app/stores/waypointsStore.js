import {createStore, applyMiddleware} from 'redux'
import {GET_WAYPOINTS, ADD_WAYPOINT, getWaypoints, addWypoint} from '../actions/waypointsActions'
import {FULFILLED, PENDING, REJECTED} from '../services/actionStatuses'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import Client from 'socket.io/lib/client'

const initialState = {
    data: []
};

const waypointsReducer = (state=initialState, action) => {

    switch (action.type) {
        case GET_WAYPOINTS + FULFILLED:
            return {data: action.payload.data};
        case ADD_WAYPOINT + PENDING:
            return {data: [...state.data, action.payload]}
    }

};


const middleware = applyMiddleware(promise(), thunk, logger());
const store = createStore(waypointsReducer, middleware);

store.dispatch(getWaypoints());

// const io = new Client('http://localhost:3000');
// io.on('gps', function (data) {
//     var waypoint = JSON.parse(data);
//     store.dispatch(addWypoint(waypoint));
// });

export default store;
