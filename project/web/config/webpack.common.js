const webpack = require('webpack');
const path = require('path');

const appPath = path.resolve(__dirname, '../app');
const webappPath = path.resolve(__dirname, '../build');
//plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// const HtmlElementsPlugin = require('./html-elements-plugin');

var config = {
    context: appPath,
    entry: {
        main: ['./app.jsx'],
        // facade: './facade.js',
        // index: 'index.js'
    },
    resolve: {
        modules:[
            appPath,
            path.resolve('node_modules')
        ],
        extensions: ['.jsx', '.js'],
        // root: appPath,
        // modulesDirectories: []
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loaders:['style-loader', 'css-loader'],

            },
            {
                test: /\.html$/,
                loader: 'raw',
                exclude: [path.resolve(appPath, 'index.html')]
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    },
    output: {
        path: webappPath,
        publicPath: '/',
        filename: '[name].js',
        library: '[name]',
        libraryTarget: 'umd'
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            children: true,
            async: true,
        }),
        new HtmlWebpackPlugin({
            title: 'Car Logger',
            template: 'index.html',
            chunksSortMode: 'dependency'
        }),
    ],
};


module.exports = config;
