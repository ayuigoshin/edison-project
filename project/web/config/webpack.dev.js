const webpack = require('webpack');
const path = require('path');

const appPath = path.resolve(__dirname, '../app');
const webappPath = path.resolve(__dirname, '../build');

const webpackMerge = require('webpack-merge'); // used to merge webpack configs
const commonConfig = require('./webpack.common.js');

const REST_CONTEXT = '/api/';
var proxy = {};
proxy[REST_CONTEXT] = {
    target: 'http://localhost:8181',
    secure: false
};

const config = webpackMerge(commonConfig, {
    devtool: 'source-map',
    entry: {
        main: ['webpack-dev-server/client?http://localhost:3000', 'webpack/hot/only-dev-server']
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['react-hot-loader', 'babel-loader']
            },
        ],
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],
    devServer: {
        port: 3000,
        host: 'localhost',
        historyApiFallback: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        public: 'localhost:3000',
        contentBase: appPath,
        hot: true,
        inline: true,
        proxy: proxy
    }
});

module.exports = config;
