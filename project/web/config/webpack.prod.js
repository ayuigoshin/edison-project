
const webpackMerge = require('webpack-merge');
const webpack = require('webpack');
const comminConfig = require('./webpack.common');


const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = webpackMerge(comminConfig, {
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['babel-loader']
            },
        ],
    },
    plugins: [

        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.LoaderOptionsPlugin({
            // test: /(\.css|\.jsx|\.css|\.html|\.scss)$/,
            minimize: true,
            debug: false
        })

    ],

});
