var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mqtt = require('mqtt');
var pg = require('pg');
var nmea = require('nmea-0183');
var dateTime = require('node-datetime');
var os = require('os');
var fs = require('fs');
var path = require('path');
var xmlBuilder = require('xmlbuilder');

var GPGGASentence = "$GPRMC,094452.123,V,5767.332,N,01195.302,E,95.6,3.31,200317,,E";

var GPGGAObject = nmea.parse(GPGGASentence);
console.log(JSON.stringify(GPGGAObject, null, 2));

var data = {
    "activityType": "RUN",
    "startTime": "2016-07-06T12:36:00Z",
    "waypoints": [
        {
            "latitude": 26.852324,
            "longitude": -80.08045,
            "elevation": 0,
            "time": "2016-07-06T12:36:00Z"
        }
    ]
};


var db = new pg.Client('postgres://postgres:1@localhost:5432/edison_project');
db.connect();
console.log(`Database connected!`);

app.use(bodyParser.json());
var server = require('http').Server(app);
var io = require('socket.io')(server);

var client = mqtt.connect('mqtt://192.168.43.29');


const REST_CONTEXT = '/api';

var port = 3000;
var devices = [];
var subscriptions = {};

app.use(express.static(path.join(__dirname, 'build')));

function subscribe(topic, callback) {
    subscriptions[topic] = callback;
}

function processMessage(topic, message) {
    var cb = subscriptions[topic];
    if (cb) {
        cb(message);
    }
}

client.on('connect', function () {
    client.subscribe('/data');
    client.subscribe('/data/info/fb');
});

client.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString());

    switch (topic) {
        case '/data/info/fb': {

            var data = JSON.parse(message.toString());
            record(data);

            get_record(function (data) {
                io.emit('gps', data);
            });

            break;
        }
        default:
            processMessage(topic, message.toString());
    }

});


app.get('/', function (req, res) {
    res.render('index.html');
});

app.get(REST_CONTEXT + '/devices', function (req, res) {
    const devices = [{unid: 1}];
    devices.push({unid: 2});
    res.send(devices);
});

app.get(REST_CONTEXT + '/data.gpx', function (req, res) {
    var tmpDir = os.tmpdir();
    get_record(function (data) {
        //convert to gpx
        const gpx = createGpx(data.map(function (item) {
            return {
                latitude: item.latitude,
                longitude: item.longitude,
                gradient:item.status,
                speed: item.speed,
                vibration: item.vibration
            }
        }));
        var filePath = path.join(tmpDir, 'data.gpx');
        fs.writeFile(filePath, gpx/*new data here*/, function (err) {
            if (err) {
                return console.log(err);
            }
            res.sendFile(filePath);

        })
    })
});

app.get(REST_CONTEXT + '/data', function (req, res) {
    get_record(function (data) {
        res.send(data)
    });

});


server.listen(port, function () {
    console.log(`Example app listening on port ${port}!`)
});

var record = function (data) {
    var dt = dateTime.create();

    var formatted = dt.format('Y-m-d H:M:S');

    console.log(dt.getTime() + ' recorded');
    db.query('INSERT INTO data_test (gps, forcex, forcey, forcez, vibration, recordDate) VALUES '
        + '(($1), ($2), ($3), ($4), ($5), ($6));',
        [data.gps, data.accelerometer.x, data.accelerometer.y, data.accelerometer.z, data.vibration, formatted]);
};


var get_record = function (callback) {
    var query = db.query('SELECT * FROM data_test ORDER BY recordDate;');
    // var query = db.query('SELECT * FROM data;');
    toReturn = [];
    query.on('row', function (row) {
        try {
            var GPGGAObject = nmea.parse(row.gps);
            console.log(JSON.stringify(GPGGAObject));

            if (GPGGAObject.longitude === 'NaN' || GPGGAObject.latitude === 'NaN' || !GPGGAObject.speed) return;
            row.latitude = Number(GPGGAObject.latitude);
            row.longitude = Number(GPGGAObject.longitude);
            row.speed = Number(GPGGAObject.speed);
            row.status = Math.abs((row.forcex / Math.sqrt((row.forcey * row.forcey) + (row.forcex * row.forcex))) * 10);
            if (GPGGAObject.valid === 'A') {
                toReturn.push(row);
            }
        } catch (e) {
            // pass
        }

    });
    query.on('end', function () {
        return callback(toReturn);
    });
};

var get_record_last_100 = function (callback) {
    var query = db.query('SELECT * FROM data_test ORDER BY recordDate LIMIT 100;');
    toReturn = [];
    query.on('row', function (row) {
        try {
            var GPGGAObject = nmea.parse(row.gps);
            console.log(JSON.stringify(GPGGAObject));
            row.latitude = Number(GPGGAObject.latitude);
            row.longitude = Number(GPGGAObject.longitude);
            row.speed = Number(GPGGAObject.speed);
            if (row.longitude === 'NaN' || row.latitude === 'NaN' || row.speed == null) return;
            row.status = Math.abs((row.forcex / Math.sqrt((row.forcey * row.forcey) + (row.forcex * row.forcex))) * 10);
            if (GPGGAObject.valid === 'A') {
                toReturn.push(row);
            }
        } catch (e) {
            // pass
        }
    });
    query.on('end', function () {
        return callback(toReturn);
    });
};
var createGpx = function (waypoints, options = {}) {

    const defaultSettings = {
        activityName: "Everyday I'm hustlin'",
        eleKey: 'elevation',
        latKey: 'latitude',
        lonKey: 'longitude',
        startTime: null,
        timeKey: 'time',
    };
    const settings = Object.assign({}, defaultSettings, options);
    const {activityName, eleKey, latKey, lonKey, startTime, timeKey} = settings;

    const gpx = xmlBuilder
        .create('gpx', {
            encoding: 'UTF-8',
        })
        .att('creator', 'Patrick Hooper')
        .att('version', '1.1')
        .att('xmlns', 'http://www.topografix.com/GPX/1/1')
        .att('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        .att('xsi:schemaLocation',
            'http://www.topografix.com/GPX/1/1 ' +
            'http://www.topografix.com/GPX/1/1/gpx.xsd ' +
            'http://www.garmin.com/xmlschemas/GpxExtensions/v3 ' +
            'http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd ' +
            'http://www.garmin.com/xmlschemas/TrackPointExtension/v1 ' +
            'http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd'
        );


    const metadata = gpx.ele('metadata');
    metadata.ele('name', 'Activity');
    if (startTime) {
        const formattedStartTime = (startTime instanceof Date) ? startTime.toISOString() : startTime;
        metadata.ele('time', formattedStartTime);
    }

    const trk = gpx.ele('trk');
    if (activityName) {
        trk.ele('name', activityName);
    }

    const trkseg = trk.ele('trkseg');

    waypoints.forEach((point) => {
        if (
            !{}.hasOwnProperty.call(point, latKey) || !{}.hasOwnProperty.call(point, lonKey)
        ) {
            throw new Error(
                'createGpx expected to find properties for latitude and longitude on all GPS ' +
                'points, but at least one point did not have both. Did you pass an array of waypoints ' +
                '(where every point has a latitude and longitude) as the first argument when you called ' +
                'the function? These properties are pretty essential to a well-formed GPX file. If they ' +
                'are found using property names different than in the default settings, you can ' +
                'override the `latKey` and `lonKey` options in the second argument to the function call.'
            );
        }

        const trkpt = trkseg
            .ele('trkpt')
            .att('lat', point[latKey])
            .att('lon', point[lonKey]);
        if ({}.hasOwnProperty.call(point, eleKey)) {
            trkpt.ele('ele', point[eleKey]);
        }
        if ({}.hasOwnProperty.call(point, timeKey)) {
            const pointTime = point[timeKey];
            const formattedPointTime = (pointTime instanceof Date) ? pointTime.toISOString() : pointTime;
            trkpt.ele('time', formattedPointTime);
        }
        if ({}.hasOwnProperty.call(point, 'vibration')) {
            trkpt.ele('vibration', point['vibration']);
        }
        if ({}.hasOwnProperty.call(point, 'speed')) {
            trkpt.ele('speed', point['speed']);
        }
        if ({}.hasOwnProperty.call(point, 'gradient')) {
            trkpt.ele('gradient', point['gradient']);
        }
    });

    // Close the `<gpx>` element and pretty format it
    return gpx.end({
        allowEmpty: true,
        indent: '  ',
        newline: '\n',
        pretty: true,
    });
};

