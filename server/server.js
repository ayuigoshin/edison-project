var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://10.42.0.1');

var cache = null;

function sendCache(data) {
    if (data instanceof Object) {
        cache = data;
    }
    io.emit('cache::update', JSON.stringify(cache));
}

client.on('connect', function () {
  client.subscribe('/data/0/temp/fb');
  client.subscribe('/data/0/led/red/fb');
  client.subscribe('/data/0/led/green/fb');
  client.subscribe('/data/0/led/blue/fb');
  client.subscribe('/data/0/music/fb');
  client.subscribe('/data/0/cfg/fb');
});

client.on('message', function (topic, message) {
  // message is Buffer 
    //console.log(message.toString());
    switch (topic) {
        case '/data/0/cfg/fb': {
            sendCache(JSON.parse(message.toString()));
            // io.emit('cache::update', cache);
            console.log(message.toString());
            break;
        }
        case '/data/0/temp/fb': {
            io.emit('temperature::update', message.toString());
            console.log(message.toString());
            break;
        }
        case '/data/0/led/red/fb': {
            io.emit('led:red::update', message.toString());
            break;
        }
        case '/data/0/led/green/fb': {
          io.emit('led:green::update', message.toString());
          break;
        }
        case '/data/0/led/blue/fb': {
            io.emit('led:blue::update', message.toString());
            break;
        }
        case '/data/0/music/fb': {
            io.emit('music:state::update', message.toString());
            break;
        }
    }
  
});

app.use(bodyParser.json());
var http = require('http');
var server = http.Server(app);
var io = require('socket.io')(server);

const REST_CONTEXT = '/api';

io.on('connection', function(socket){
  console.log('a user connected');
  sendCache();
});

app.post(REST_CONTEXT + '/msg', function (req, res) {
    client.publish('/data/0/msg/rq', JSON.stringify({msg: req.body.msg}));
    res.send();
});
app.post(REST_CONTEXT + '/led/red', function (req, res) {
    client.publish('/data/0/led/red/rq', JSON.stringify({value: req.body.value}));
    res.send();
});
app.post(REST_CONTEXT + '/led/blue', function (req, res) {
    client.publish('/data/0/led/blue/rq', JSON.stringify({value: req.body.value}));
    res.send();
});
app.post(REST_CONTEXT + '/led/green', function (req, res) {
    client.publish('/data/0/led/green/rq', JSON.stringify({value: req.body.value}));
    res.send();
});

app.post(REST_CONTEXT + '/music', function (req, res) {
    client.publish('/data/0/music/rq', JSON.stringify({state: req.body.state}));
    res.send();
});

app.get(REST_CONTEXT + '/temp', function (req, res) {
    client.publish('/data/0/temp/rq', null);
    res.send();
});

server.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});
